import { 
	setCarData, 
	search, 
	filterCar,
	resetObject 
} from '../src/assets/cars'

describe('filter of cars', () => {
	test('obj assignment 1', () => {
		const car = new setCarData(search)
		car.setData('brand', 'BMW').setData('year', 2020)

		expect(search).toEqual(expect.objectContaining({ brand: 'BMW', year: 2020 }))
		expect(filterCar()).toEqual([
			{
				brand: 'BMW',
				model: 'Serie 3',
				year: 2020,
				price: 30000,
				doors: 4,
				color: 'Blanco',
				transmission: 'automatico'
			}
		])
	})

	test('obj assignment 2', () => {
		resetObject(search)

		const car2 = new setCarData(search)
		car2
			.setData('brand', 'Dodge')
			.setData('year', '2020')
			.setData('min', 20000)
			.setData('max', 30000)
			.setData('doors', 2)
			.setData('color', 'Rojo')
			.setData('transmission', 'manual')
		
		expect(search).toEqual(expect.objectContaining({ min: 20000, max: 30000, year: 2020 }))
		expect(filterCar()).toEqual([
			{ 
				brand: "Dodge",
				model: "Challenger",
				year: 2020,
				price: 25000,
				doors: 2,
				color: "Rojo",
				transmission: "manual"
		 	}
		])
	})
})