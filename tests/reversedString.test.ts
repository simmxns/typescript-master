import { reversedString } from '../src/specials/wars/reversedString'

describe("Passing a string a return it reversed", () => {
	test('toBe & not toBe', () => {
		expect(reversedString('world')).toBe('dlrow')
		expect(reversedString('hello')).toBe('olleh')
		expect(reversedString('')).toBe('')
		expect(reversedString('h')).toBe('h')
		expect(reversedString('potato')).not.toBe('potato')
	})
})