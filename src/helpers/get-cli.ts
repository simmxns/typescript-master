/**
 * 
 */
export function getCommandLine(): string {
	const system: {
		[value: string]: any
	} = {
		'darwin': () => 'open',
		'win32': () => 'start',
		'linux': () => 'open'
	}
	const DEFAULT_OPTION: string = 'unknow way to open your file'
	return system[process.platform] || DEFAULT_OPTION
}