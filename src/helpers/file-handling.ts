import FileSystemValidator from '@/validators/FileSystemValidator'
import fs from 'fs'
import os from 'os'
import path, { dirname } from 'path'

export interface ModeFile {
	[key: string]: string
	home: string
	dir: string
}

export interface OpenFileAttrs {
	mode?: 'home' | 'dir'
	openMode?: 'node' | 'system'
}

const fsv = new FileSystemValidator()

/**
 * If the file exists will be open to read, otherwise it will be created
 *
 * @param file your file, example: `sample.txt`
 * @param directory the directory of your file, example: `src/assets` the last and the starting slash aren't necessary
 * @param attr is an optional parameter specify extra options { mode?, openMode? }
 * 
 * `attr`:
 * possible modes:
 * * `home` `os.homedir()` start in the home of the os (user folder)
 * * `dir` `dirname()` start at the src directory of your project
 *
 * possible open modes:
 * * `node` `fs.open()` use the node file to open the file
 * * `system` `unknow` exec command to open your file in the os 
 */
export function openFile(
	this: any,
	file: string,
	directory: string,
	attr?: OpenFileAttrs
): string {
	this.file = file
	this.directory = directory
	const DEFAULT_MODE = 'home'
	this.mode = attr!.mode ?? DEFAULT_MODE
	const DEFAULT_OPENMODE = 'node'
	this.openMode = attr!.openMode ?? DEFAULT_OPENMODE
	let root: string

	const modeOptions: ModeFile = {
		home: (root = os.homedir()),
		dir: (root = dirname(require.main!.filename))
	}
	root = modeOptions[this.mode]

	const completePath = `${root}/${directory}/${file}`

	if (!fsv.isFileAndDirectory(completePath)) throw new Error('no such file or directory...')
	
	if (this.openMode == 'node') {
		fs.open(completePath, 'r', (err, fd) => {
			if (err) throw `${err.code}> ${err.message}`
			console.log(`${file} successfully open!`)
		})
	}

	if (this.openMode == 'system') {
		console.log(`executing command for open ${file}...`)
	}

	return completePath
}

export function writeFile(file: string): void {
	const fileName = path.basename(file)
	const stats = fs.lstatSync(file)

	if (!stats.isDirectory()) throw new TypeError(`${fileName} must be path`)

	console.log(`${file} saved!`)
}

export function closeFile(file: string): void {
	const fileName = path.basename(file)
	const stats = fs.lstatSync(file)

	if (!stats.isDirectory()) throw new TypeError(`${fileName} must be path`)

	//fs.close(openFile(file))
	console.log('the file has been closed')
}
