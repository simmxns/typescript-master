import { useState } from "react";

interface Counter { 
	counter: number, 
	increment: (options?: { limit?: number, value?: number }) => void, 
	decrement: (options?: { limit?: number, value?: number }) => void, 
	restart?: (value?: number) => void
}

/**
 * useCounter (useState)
 * 
 * Return a value and a function for modify that value
 * 
 * Recieve a number and modify the value to increment the number or decrement it
 * 
 * @param initialValue default `0` the initial value of the element that will be alter
 */
export function useCounter(initialValue: number = 0): Counter {
	const [counter, setCount] = useState(initialValue)

	const increment = (options?: { limit?: number, value?: number }) => {
		const { limit, value } = options!
		if (counter >= limit!) {
			return setCount(value!)
		}
		return setCount(counter + 1)
	}
	const decrement = (options?: { limit?: number, value?: number }) => {
		const { limit, value } = options!
		if (counter <= limit!) {
			return setCount(value!)
		}
		return setCount(counter - 1)
	}
	const restart = (value?: number) => setCount(value || 0)

	return {
		counter,
		increment,
		decrement,
		restart
	}
}