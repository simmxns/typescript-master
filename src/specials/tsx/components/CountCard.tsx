import React, { useRef } from 'react'
import { useCounter } from '@H/useCounter'
import { rotateOnDown, rotateOnUp } from '@/helpers/rotateCard'

export default function CountCard (): JSX.Element {
	const { counter, increment, decrement } = useCounter()
	const card = useRef<HTMLDivElement>(null)

	// use effect runs when the component render completely

	const rotateCardActive = (e: React.MouseEvent): void => {
		const { current: element } = card
		rotateOnDown<HTMLDivElement>(e, element!, {
			classOne: 'rotateUp',
			classTwo: 'rotateDown',
			matcherA: 'a',
			matcherB: 'b'
		})
	}
	const rotateCardInactive = (e: React.MouseEvent): void => {
		const { current: element } = card
		rotateOnUp<HTMLDivElement>(e, element!, {
			classOne: 'rotateUp',
			classTwo: 'rotateDown',
			matcherA: 'a',
			matcherB: 'b'
		})
	}

	return (
		<div className="card" ref={card}>
			<button
				onClick={increment}
				onMouseDown={rotateCardActive}
				onMouseUp={rotateCardInactive}
				className="btn-card a"
			/>
			<h1>{counter}</h1>
			<button
				onClick={decrement}
				onMouseDown={rotateCardActive}
				onMouseUp={rotateCardInactive}
				className="btn-card b"
			/>
		</div>
	)
}