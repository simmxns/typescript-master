import React from 'react'

export const Heart = (props: any) => {
	return (
		<button className="heart" onClick={props.onClick}>
			<svg width={28} height={25} fill="none">
				<path
					d="M24.796 13.581 14.002 24.375 3.21 13.58C.3 10.675.299 6.128 3.208 3.206l.001-.001a7.327 7.327 0 0 1 5.194-2.168c1.823 0 3.58.685 4.922 1.92l.677.622.677-.623A7.276 7.276 0 0 1 22.411 1.6a7.32 7.32 0 0 1 2.38 1.6l.709-.705-.709.705c2.916 2.928 2.912 7.475.005 10.381Z"
					stroke="#353535"
					strokeWidth={2}
				/>
			</svg>
		</button>
	)
}
