import type { T, X } from "@/handbook/types/arrayDiff.types"

export function diff2Lists(first: Array<T>, second: Array<T>): T[] {
	const result: T[] = []
	for (let i = 0; i < first.length; i++) {
		if (second.indexOf(first[i]) == -1) {
			result.push(first[i])
		}
	}
	for (let i = 0; i < second.length; i++) {
		if (first.indexOf(second[i]) == -1) {
			result.push(second[i])
		}
	}
	return result
}

export function diffAnyList(...array: Array<X>): T[] {
	array.forEach(v => console.log(v))
	return []
}

