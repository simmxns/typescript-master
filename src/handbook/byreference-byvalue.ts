let value = 5
export let value2 = value // by value
value2 = 10

let arr = [1,2,3,4]
// let arr2 = arr // by reference
let arr2 = [ ...arr ]
arr2.pop()

// console.log(value, value2) // 5 10
// console.log(arr, arr2) // [1,2,3,4] [1,2,3]
