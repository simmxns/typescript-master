/**
 *`THIS` keyword
 * 
 * this === current execution context
 * 
 * Example:
 * ```js
 * function whodis() {
 * 	this
 * }
 * var.whodis() // var
 * ```
 * 
 * In 'browsers' print this at the fisrt level of your
 * will show you the object window
 * 
 * The same in 'node' will print the global object
 * Same as 'deno'
 */

export function whodis(this: any) {
	//this -> undefined
}

const me = {
	name: 'Simon',
	face: '😜',
	whodis 	
}

me.whodis() // this -> { name: 'Simon', face: '😜', whodis: [Function: whodis] }

const itsMe = whodis.bind(me)
itsMe() // this -> undefined

export const sandra = {
	face: '😝',
	name: 'Sandra',
	whodis: function (this: any) {
		console.log(this) // this -> { face: '😝', name: 'Sandra', whodis: [Function: whodis], butWhoAmI: [Function: butWhoAmI] }
	},
	butWhoAmI: () => {
		//console.log(this) this -> Global | Window, 
		//            ^ Error The containing arrow function captures the global value of 'this'
	}
}

export function showFace(this: any) {
	return this.face
}

const simon = {
	face: '🤯'
}

const showSimonsFace = showFace.bind(simon) // this scope changed to obj simon
showSimonsFace() // '🤯'

//showFace.call(simon, 1, 2, 3) // this is given, this means that this is now referece on this obj
//showFace.apply(simon, [1, 2, 3]) // apply are the same that call but extra args are into an array

/* Functional you dont need to use the confusing this
export const Fun = (props: any) => (
	<div>
		<h1>Hello, {props.name}</h1>
	</div>
)
*/

/* OOP 
class Classic extends Component {
	constructor(props: any) {
		super(props);
		this.state = {
			myState: true
		}
	}
}
*/

// Best way to use this?
export const Horse = function (this: any, name: string): any {
	this.name = name
	//this -> Horse {}
	this.sayHello = function() {
		console.log(`Hi ${this.name}!`)
	}
	// arrow function cannot have a 'this' parameter
	this.whodis = () => {
		console.log(this) // this -> { name: 'Sandrita', sayHello: [Function (anonymous)], whodis: [Function (anonymous)] }
	} 
	this.poop = function () {
		console.log('💩 💩 💩')
		// if you want to chaining you need to return this in the method you want to chain
		return this
	}
} as any as { new (name: string): any }

// new create objects where this is the new object
//const myHorse = new (Horse as any)('Simoncin')
const myHorse = new Horse('Sandrita')
// method chaining
myHorse.poop().poop().poop()