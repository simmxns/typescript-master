/**
 * Higher order functions
 * 
 * Are a functions that can return another function, or you can pass a function
 * as an argument
 * 
 * ```ts
 * const laser = (intensity: number, f: Function): string => `${intensity + f()}`
 * 
 * console.log(laser(2, () => ' volts!' )) // 2 volts!
 * ```
 * 
 */
export function love(favoriteColor: string) {
	return function(match: string) {
		return favoriteColor === match ? 'We match!' : 'Sorry you are not my type'
	}
}

love('Red')('Red') // 'We match!'

const sandraAlwaysSay = love('This is private')
sandraAlwaysSay('Green') // Sorry you are not my type

export const rose4MyCrush = (girlName: string) => (roseAmount: number) => {
	const isSandra = () => {
		if (girlName.includes('Sandra')) {
			return roseAmount > 1 ? `${roseAmount} Roses` : `${roseAmount} Rose`
		}
		return 'Sorry you broke my heart 💔'
	}
	return isSandra()
}

const myCrush = rose4MyCrush('Sandra L')
myCrush(1000)

