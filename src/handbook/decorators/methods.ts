/**
 * Method Decorator
 * 
 * Reciben 3 argumentos
 * 
 * Primero el prototipo de la clase del metodo (como las propiedades)
 * 
 * Segundo nombre del metodo
 * 
 * ECMA > 4 
 * Tercero descriptor de propiedades (descriptor del metodo)
 * 
 * Descriptor API {
 *   configurable: puede cambiarse y eliminarse la propiedad del objeto correspondiente
 *   enumerable: si y solo si la propiedad se muestra durante la enumeracion de las propiedades del objeto correspondiente
 *   value: el valor asociado a la propeidad
 *   writable: true para ser cambiado con un operador de asignación
 *   get/set
 * }
 */
export function MethodDecorator(target: any, propertyKey: string, descriptor?: any): any {
	const originalFunction = target[propertyKey]

	const decoratedFunction = function() {
		originalFunction()
		console.log(`${propertyKey} has executed`)
	}
	descriptor.value = decoratedFunction

	return descriptor
}

/**
 * Parameters decorators
 *
 * 3 argumentos
 * 
 * primero objecto actual
 * 
 * segundo nombre de la propiedad
 * 
 * tercero posicion del argumento
 * 
 */
export function ParamDecorator(target: Object, propertyKey: string, paramIndex: number) { 
	console.log('method name', propertyKey);
	console.log('Class', target);
	console.log('index', paramIndex);
}

class Speaker {
	@MethodDecorator n(a: number, @ParamDecorator b: number): number { 
		return a + b 
	}
}

const speaker = new Speaker()
speaker.n(20, 14)
speaker.n(14, 20)