/*
 @Decorators
 Es un patron de programacion en el que tomamos algo (una: clase, prop, metodo)
 Y lo envolvemos para asignarle informacion o funcionalidad adicional a aquello que se esta envolviendo
 El objetivo es extender la funcionalidad de un componente sin modificarlo permanentemente
 !Experimental
*/

/*
 El que recibimos es una function que representa el constructor de la clase
 esto no quiere decir que recibimos la function constructora
 sino es una funcion con la que se crean los objetos
 si muy confuso...
 las funciones son el rey por eso se recibe una funcion como parametro
*/
function Decorator(target: Function) {
	console.log("class decorator is running...")
	target.prototype.className = target.name
}

@Decorator
export /* or @Decorator */ class Speaker {}