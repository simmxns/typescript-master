/* Interfaces
 * Es un esqueleto es una plantilla vacia que los objetos deben de cumplir para poder implementarla
 */
export interface Asset {
	x: number,
	y: number,
	width: number,
	height: number,
	getCoords(): string
}

export class Hero implements Asset {
	x: number
	y: number 
	width: number
	height: number

	getCoords(): string {
		return `${this.x} & ${this.y}`
	}
}

export class Bullet implements Asset {
	x: number
	y: number 
	width: number
	height: number

	getCoords(): string {
		return `${this.x} & ${this.y}`
	}
}

export class Enemy implements Asset {
	x: number
	y: number 
	width: number
	height: number

	getCoords(): string {
		return `${this.x} & ${this.y}`
	}
}

export class SpaceBullet extends Bullet {}
// let spacebullet: SpaceBullet = new SpaceBullet()

export class Collisions {
	static check(obj: Asset, obj2: Asset) {
		// Validar que exista forma de comparar los elementos
		// Compare obj with obj2
		if (obj.getCoords == obj2.getCoords) return "Colliding"
		return "No collision"
	}
}
