
export interface Search {
	brand: string
	year: string
	min: string
	max: string
	doors: string
	color: string
	transmission: string
}