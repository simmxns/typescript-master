/** Interface for a simple Quote */
export interface Quote {
	quote: string
	author: string
}