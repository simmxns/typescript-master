/**
 * Generics
 * 
 * Tipo flexible que te permite trabajar con diferentes tipos
 * 
 * Es como una variable de tipos de datos
 */

export function genericRecetor<T>(obj: T): T {
	return obj
};

genericRecetor<string>("Simon")
const num = genericRecetor<number>(19)
// num = "Hello"
//        ^ must be number because the generic is number

function printAll<T>(arr: T[]): void {
	console.log(arr.length)
}

printAll<string>(["Hi", "Simon"])
printAll<number>([20, 25, 12])
printAll<boolean>([true])

class Printer<T> {
	printAll(arr: T[]) {
		console.log(arr.length)
	}
}

const printer: Printer<number> = new Printer()
printer.printAll([20, 25, 12])