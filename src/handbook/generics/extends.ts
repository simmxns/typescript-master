/**
 * Extending generics
 * 
 * When you extend a generic that mean that you are limiting the data types that you can pass in that generic
 *  
 * { name: string } is an obj where T must to match
 */
export class Collection<T extends { name: string }> {
	protected items: T[] = []
	
	constructor(items: T[]) {
		this.items.push(...items)
	}

	add(items: T) {
		this.items.push(items)
	}
	remove(index: number) {
		this.items.slice(index, 1)
	}
	getItem(index: number): T {
		return this.items[index]
	}
}

export class SearchableCollection<T extends { name: string}> extends Collection<T> {
	find(name: string): T | undefined {
		return this.items.find(item => item.name === name)
	}
}

interface Person {
	name: string
	age: number
}

/** 
 * Person has the name string property and the age number property, but we can still use it.
 * As long as the object has the name property and its value is a string, we can use it.
 */
const people: SearchableCollection<Person> = new SearchableCollection<Person>([
	{
		name: "Simon",
		age: 19
	},
	{
		name: "Sandra",
		age: 18
	}
]) 

interface Asset {
	x: number
	y: number
}

interface AssetGeneric<T> {
	x: number
	y: number
	generic: T
}

function generic<T extends Asset>(obj: T) {}

// generic<number>(20)
//         ^ Type 'number' does not satisfy the constraint 'Asset'

class Point implements AssetGeneric<string> {
	x: number
	y: number
	generic: string
}

class Elements implements AssetGeneric<Point> {
	x: number
	y: number
	generic: Point
}

generic<Point>(new Point())