/**
 * Array.splice()
 * 
 * change or replace elements into an array
 * replace elements the second param have a number
 * NOTE: the elements will be added after the described index
 */

export const splice = ['Jan', 'Apr', 'May', 'Jun', 'Jul']
splice.splice(1, 0, 'Feb', 'Mar')
splice // ["Jan", "Feb", "May", "Apr", "May", "Jun", "Jul"]
splice.splice(6, 1, 'Ago', 'Sep') 
splice // ["Jan", "Feb", "May", "Apr", "May", "Jun", "Ago", "Sep"] 

