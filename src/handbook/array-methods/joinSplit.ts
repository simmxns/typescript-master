/**
 * Array.join()
 * 
 * merge all the elements of an array
 */

export const join: string[] = ['Welcome', 'to', 'typescript']

let joined = join.join(' ') // "Welcome to typescript"

/**
 * String.split()
 * 
 * String to array with a separator and a limit 
 * limit must be minus of the total words, because it will make that your array have that number of pos
 */

export let split = joined.split(' ') // ['Welcome', 'to', 'typescript']
joined.split(' ', 2) // ['Welcome', 'to']
