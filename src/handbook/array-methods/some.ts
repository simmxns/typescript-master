/**
 * Array.some()
 * 
 * Check if some of the values meet the condition
 * 
 * NOTE: empty arrays return false
 */

export const some = [1, 2, 3, 4, 5]

some.some(values => values % 2 === 0) // true