/**
 * Creates an instance representing an error that occurs when a numeric variable or parameter is outside of its valid range.
 */
throw new RangeError('Invalid array length')
//console.log.apply(console, new Array(1000000000)); // RangeError: Invalid array length

/**
 * Creates an instance representing an error that occurs when de-referencing an invalid reference. e.g.
 */
throw new ReferenceError('notValidVar is not defined')
//console.log(notValidVar);

/**
 * Creates an instance representing a syntax error that occurs while parsing code that isn't valid JavaScript.
 */
throw new SyntaxError('Unexpected token *')
//1***3;

/**
 * Creates an instance representing an error that occurs when a variable or parameter is not of a valid type.
 */
throw new TypeError('"1.2".toPrecision is not a function')
//('1.2').toPrecision(1);

/**
 * Creates an instance representing an error that occurs when encodeURI() or decodeURI() are passed invalid parameters.
 */
throw new URIError('URI malformed')
//decodeURI('%');

// ALWAYS USE ERROR!

export {}