import util from 'util'
// callback to promise
const sleep = util.promisify(setTimeout)

export async function taskOne() {
	try {
		throw new Error('Something went wrong')
		await sleep(3000)
		return 'ONE VALUE'
	} catch (e) {
		console.error(e)
	}
}

export async function taskTwo() {
	await sleep(2000)
	return 'TWO VALUE'
}
