import { openFile, writeFile, closeFile } from '@/helpers/file-handling'

/**
 * `try..catch`
 * 
 * "try" executes the code inside it and signals if there is an exception in that case "catch" will catch it and allow to run the next codes
 * 
 * `finally`
 * 
 * Is executed after the `try`, `catch` clauses and is executed whether or not an exception was thrown
 * 
 * possible combinations
 * 
 * `try..finall`
 * 
 * `try..catch..finall`
 */
export const exc = 'Awesome Exception!'

try {
	throw exc
} catch (e) {
	if (e instanceof TypeError) console.log('seis a type error')
	if (e instanceof RangeError) console.log('e is a range error')
	if (e instanceof EvalError) console.log('e is a eval error')
	console.log(e)
}

export function exceptionFinally (file: string, directory: string): void {
	openFile(file, directory, { mode: "dir" })
	try {
		// holds a resource
		writeFile(file)
	}
	finally {
		// always closes
		closeFile(file)
	}
}


export function isValidJSON(json: string): boolean {
	try {
		JSON.parse(json)
		return true
	} catch (e) {
		return false
	}
}