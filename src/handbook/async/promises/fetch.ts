import 'isomorphic-fetch'
import $ from 'jquery'

/**
 * Promises & Fetch API
 * 
 * fetch y las promesas resuelven un problema de ya hace tiempo que pasaba
 * cuando se trabajaba con el continuation passing style o cuando se le pasa
 * un callback a la funcion
 */
// new
fetch('https://jsonplaceholder.typicode.com/posts')
	.then(data => {
		if (data.status >= 400) throw new Error('Bad response server')
		return data.json()
	})
	.then(data => console.log(data))

// old (continuation passing style and bad errors handling)
$.get('https://jsonplaceholder.typicode.com/posts', data => {
	console.log(data)
	$.get('https://jsonplaceholder.typicode.com/users', data => {
		console.log(data)
	})
}, "json")

$.ajax({
	url: 'https://aws.random.cat/meow?ref=apilist.fun',
	dataType: "application/json",
	success: data => {
		console.log(data)
	}	
})
