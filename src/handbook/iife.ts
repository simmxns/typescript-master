/* 
 Immeditely invoked function expressions 
 or Self executing functions
 this functions runs as soon it is defined, and contain two major parts
 * first: anonymous function with lexical scope enclosed within 'Grouping Operator' ().
 This prevents accessing variables whitin the IIFE idiom as well as polluting
 the global scope
 * second: create the iife expression () through which js engine will directly
 interpret the function
*/

const getFileStream = async (url: string) => url;

// avoid polluting the global namespace
(function() {
	// some initiation code
	let firstVar: any
	let secondVar: any
	console.log("I never got called but I run anyway ")
})();
// firstVar and secondVar will be discarded after the function is executed

// module pattern
export const makeWithDraw = (balance: number) => ((copyBalance: number) => {
	let balance = copyBalance
	const doBadStuff = () => {
		console.log('Doing bad things with your money 😈...')
	}
	doBadStuff()
	return {
		withdraw(amount: number) {
			if (balance >= amount) return balance -= amount
			return 'Insufficient money'
		}
	}
})(balance);

// execute an async function
(async () => {
	const stream = await getFileStream('https://poetrydb.org/author')
	for await (const data of stream) {
		console.log({ data })
	}
	return stream
})();