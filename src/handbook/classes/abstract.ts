/* Clases abstractas
 * 1. una clase abstracta no se instancia
 * 2. pueden contener metodos abstractos (no deben contener implementacion)
 */
abstract class Asset {
	x: number
	y: number
	width: number
	height: number
	getCoords(): string { return `x:${this.x}, y:${this.y}`}

	abstract move(speed: number): boolean
}

// Al una clase tener es propiedades heredadas abstractas todas estas deberan ser declaradas en la subclase
class Speed extends Asset {
	move(speed: number) { return true }
}

export default Asset