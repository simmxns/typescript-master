export class Video {
	public title: string // Automaticamente un atributo siempre será public de no especificarsele
	private totaltime: number // Solo su clase puede acceder a un elemento privado
	protected author: string // Clases y subclases pueden acceder a los elementos protegidos

	constructor(title: string, totaltime: number, author: string) {
		this.title = title
		this.totaltime = totaltime
		this.author = author
	}
}

class OtherVideo extends Video {
	printAuthor(): void {
		console.log(this.author) // Correct
	}
}

const video: Video = new Video("Title", 20, "Simon")
// console.log(video.totaltime) // La propiedad es privada por ende no puedo llamarla en otro lugar que no sea su propia clase