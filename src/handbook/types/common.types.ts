import { 
	ComplicatedJoke as HardJoke, 
	Joke, 
	Quote, 
	Riddle 
} from '@/handbook/interfaces/barrel';

/** Fact types per kind of content */
export type Fact = Quote | Quote[] | Joke | HardJoke | Riddle | string
