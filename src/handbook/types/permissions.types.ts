export enum DocPermissions {
	edit = 4,
	comment = 2,
	view = 1
}