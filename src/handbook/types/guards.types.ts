/*
	Sirve para encapsular tu codigo en un solo tipo de dato posible
	Su finalidad es aprovechar los union types sin perder la certeza
	que nos da el tipado estático.
*/

function isNumber(obj: number | string ): obj is number {
	return typeof obj === 'number'
}

function isString(obj: number | string ): obj is string {
	return typeof obj === 'string'
}

export function getAge(age: number | string) {
	if (typeof age === 'number') {
		// Age is number!
	} /* else kw is optional! */ {
		// Age is string!
	}
}

interface A {
	x: number
}
interface B {
	y: number
}
interface C {
	x: string
}

/**
 * in Operator
 * 
 * In typescript the in operator acts as a type guard,
 * with the difference that it will evaluate if an element is in an object
 */
export const coords: A | B | C = {
	x: 20,
	y: 22,
}

if ('x' in coords) {
	// q is A or C
	//coords.x: string | number
} /* else */ {
	// q is B
}
