export type finiteTuple = [string, number, boolean]
export type infiniteTuple = [string, number, boolean, ...(string | number | boolean)[]] 
//export type infiniteTuple = [string, number, boolean, ...Array<string | number | boolean>] 