import OnlyNumbersValidor from '@/validators/OnlyNumbersValidator'
import type { Search } from '@/handbook/interfaces/search.interface'
import cars from './carsdb.json'

const numberValidation = new OnlyNumbersValidor()
export const search: Search = {
	"brand": "",
	"year": "",
	"min": "",
	"max": "",
	"doors": "",
	"color": "",
	"transmission": ""
}

/** When you need to reset the object filtered just run this function */
export const resetObject = function (obj: object): void { 
	for (let index in obj) {
		Object.defineProperty(obj, index, { value: '' })
	} 
}

/** 
 * Return the result obtained by filtering the cars 
 * 
 * @param carsResult when you filter the cars you recieve a new object or a string telling you that nothing was found
 * @retuns the cars filtered or string for show you that the search does not match
 * */
export const showResults = (carsResult: object[] | string): Array<object> | string => carsResult

/** Filter the filled object with the entries of `setCarData()` */
export const filterCar = function (): Array<object> | string {
	const result = cars
		.filter(filterYear)
		.filter(filterBrand)
		.filter(filterMinPrice)
		.filter(filterMaxPrice)
		.filter(filterDoors)
		.filter(filterColor)
		.filter(filterTransmission)

	return result.length ? showResults(result) : showResults('Not cars found')		
}
	
/**
 * A function for add elements into an object and exec a callback for filter the new object
 * 
 * @constructor
 * @param obj The object to be modified
 * @param key Key of the object that will be altered
 * @param value The value of data
 * 
 * @function setData This function put the data you added into an object
 * @returns Returns 'obj' with the added value(s)
 * @callback filterCar When the data is added filterCar() for fill the new object
 */
export const setCarData = function (this: any, obj: { [key: string]: any }) {

	this.setData = function (key: string, value: string | number) {
		if (numberValidation.isRegExpNumber(value)) value = +value

		Object.defineProperty(obj, key, { value: value })
		filterCar()
		return this
	}
} as any as { new (obj: { [key: string]: any }): any }

// values matcher
function filterBrand (car: any) {
	const { brand } = search
	if (brand) {
		return car.brand === brand 
	}
	return car
}

function filterYear (car: any) {
	const { year } = search
	if (year) {
		return car.year === year 
	}
	return car
}

function filterMinPrice (car: any) {
	const { min } = search
	if (min) {
		return car.price >= min
	}
	return car
}

function filterMaxPrice (car: any) {
	const { max } = search
	if (max) {
		return car.price <= max 
	}
	return car
}

function filterDoors (car: any) {
	const { doors } = search
	if (doors) {
		return car.doors === doors 
	}
	return car
}

function filterColor (car: any) {
	const { color } = search
	if (color) {
		return car.color === color 
	}
	return car
}

function filterTransmission (car: any) {
	const { transmission } = search
	if (transmission) {
		return car.transmission === transmission 
	}
	return car
}