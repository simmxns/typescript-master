/**
 * Optional chaining ?
 * Allow work with objects an arrays with many dimensions without worriding if some part
 * of this one are not declared
 * if a reference is near-null (null or undefined), the expression does a short-circuit
 * evaluation with a return value of undefined
 */

const adventurer = {
  name: 'Alice',
  cat: {
    name: 'Dinah'
  }
};

const dogName = adventurer.dog?.name;

