// Put multiple values just passing one array in a function

/*function process( item1: string, item2: string, item3: string) {
 console.log(item1, item2, item3) 
}
*/
export function process(...values: number[]): void {
	values.forEach(v => {
		console.log(v) // 1,2,3,4,5
	})
}

process(1,2,3,4,5)
