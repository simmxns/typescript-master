// Copy of an array
const arr = [ 2, 4, 6, 8 ]
export const arr2 = [ ...arr, 10, 12, 14 ]
// console.log(arr, arr2) // [2,4,6,8] [2,4,6,8,10,12,14]

interface IItem {
	id: number,
	title: string,
	date?: Date,
}
export const item: IItem = {
	id: 5,
	title: 'Hola',
}
const item2 = { ...item };

item2.date = new Date('2021/12/28')

// console.log(item, item2) // {5, 'Hola'} {5, 'Hola', '2021-12-28T03:00:00.000Z'}
