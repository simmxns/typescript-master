// old
export let old = function(x: number ,y: number): number {
	return x * y
}

// es6
/* Arrow function 
	In one line or wrapper by parenthesis will return the value automatically
	do not have their own this
	must be initialized before being used
 */
export const arrowFunc = (x: number, y: number): number => x * y
