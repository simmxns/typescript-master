// from here to top strict mode is off
"use strict";
/**
 * `"use strict";`
 * 
 * Switch javascript to strict mode.
 * 
 * What are some of the rules of the strict mode?
 * 
 * "use strict" is usually located at the top of the file, 
 * but if it is on another line of code, it will compile
 * all the above code as normal javascript
 * 
 * Why Strict Mode?
 * 
 * Strict mode makes it easier to write "secure" JavaScript.
 */

// everything without declaring first is not allowed
noDeclaredVar = 20 // x is not defined

noDeclaredObj = {
	p1: 10,
	p2: 20
}

function noDeclaredInsideFunc() {
	x = 3.14 // not allowed
}

let deletedVar = 3.14
// the use of delete it is not allowed
delete deletedVar
delete noDeclaredInsideFunc

function duplicatedParams(p1, p1) {} // not allowed duplicated name parameters

// octal numeric literals and space octal chacteres are not allowed
let octal = 010 
let octalEsc = "\010"

// writing to a read-only property is not allowed
const readOnlyObj = {}
Object.defineProperty(readOnlyObj, "x", { value: 0, writable: false })
readOnlyObj.x = 3.14

// writing to a get-only property is not allowed
const getOnlyObj = { get x() { return 0 } }
getOnlyObj.x = 3.14

// delete the removable properties are not allowed
delete Object.prototype

/* 
 with statement is not allowed
 NOTE: The with statement extends the scope chain for a statement
*/
with (Math) {
	x = cos(2)
}

// for security reasons, eval() is not allowed to create variables in the scope from which it was called
eval ("let x = 2")

// reserved words as a variable names are not allowed
let eval = 3.14
let arguments = 3.14
// and futures
let implements = 3.14
let interface = 3.14
let let = 3.14
let package = 3.14
let private = 3.14
let protected = 3.14
let public = 3.14
let static = 3.14
let yield = 3.14

/** 
 * this on "use strict";
 * 
 * `this` behave if different when strict mode is on
 * 
 * `this` refers to the object that called the function 
 * 
 * If the object is not specified, functions in strict mode 
 * will return undefined and functions in normal mode will
 * return the global object (window)
*/

export function thisUndefined() {
	console.log(this) // undefined
}


