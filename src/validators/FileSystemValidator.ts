import fs from 'fs'
import os from 'os'
import { dirname } from 'path'

type startDir = {
	mode?: 'home' | 'root' | 'none'
}

console.log(dirname(require.main!.filename))

export default class FileSystemValidator {
	private ROOT = dirname(require.main!.filename)
	private HOME = os.homedir()

	public fileExists(
		path: string,
		options: startDir = { mode: 'root' }
	): boolean {
		const { mode } = options

		if (mode === 'root') path = this.ROOT + path
		if (mode === 'home') path = this.HOME + path
		console.log(path)
/*		if (fs.lstatSync(path).isFile() && fs.lstatSync(path).isDirectory())
			return true*/
		return false
	}
}
